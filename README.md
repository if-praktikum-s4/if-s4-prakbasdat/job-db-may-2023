# Job Vacancy - Database Administrator

## Senior Database Administrator Yayasan Bina Nusantara
![](https://rekreartive.com/wp-content/uploads/2018/10/Logo-Binus-University-Universitas-Bina-Nusantara-Original-PNG.png)
Informasi lebih detail bisa kunjungi [via JobStreet](https://www.jobstreet.co.id/database-jobs/in-Indonesia)
## Job Description

1. Membuat database & perencanaan kegiatan sistem, monitoring dan evaluasi untuk semua Unit/ Divisi Bisnis BINUS.
2. Bertanggung jawab untuk mendukung semua Database Server, dan memastikan kinerja, ketersediaan, dan keamanannya.
3. Memastikan efektivitas dan efisiensi operasional database dan administrasi sistem.
4. Memantau aktivitas operasional database dan sistem di seluruh Unit/Divisi Bisnis BINUS.
5. Menjaga keamanan database di seluruh Unit/Divisi Bisnis BINUS.
6. Mendukung ketersediaan dan pengoperasian Oracle ERP dan database Analytic untuk kegiatan operasional yang dilakukan oleh unit-unit di lingkungan Unit BINUS
7. Memberikan layanan bermutu kepada pengguna, terkait dengan otorisasi database, perubahan dan penarikan

## Salary in Month
1. **Minimum**     : Rp. 1.800.000 
2. **Maximum**     : Rp. 5.500.000
#### 
## Requirement
1. Gelar Sarjana Minimum dalam Ilmu Komputer atau bidang terkait
2. Minimal 2 tahun pengalaman sebagai Administrator Database atau bidang terkait.
3. Memahami bahasa query terstruktur (SQL)
Pengetahuan tentang "Sistem Manajemen Basis Data Relasional" (RDBMS), 'Sistem Manajemen Basis Data Berorientasi Objek' (OODBMS) dan sistem manajemen basis data XML
4. Mata untuk detail dan akurasi
5. Memiliki pengalaman dengan perangkat lunak database/aplikasi web mereka
6. Pengetahuan terkini tentang teknologi dan Undang-Undang Perlindungan Data
7. Kemampuan untuk bekerja dengan baik di lingkungan yang serba cepat, di mana teknologi terus berubah
## Additional Information
### Career Level
Staff (non-management & non-supervisor)
### Qualification
Bachelor's Degree
### Years of Experience
2 years
### Job Specializations
- Computer/Information Technology
- IT-Network/Sys/DB Admin
